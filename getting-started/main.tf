terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "fashiondeluxe"
  }


   workspaces {
      name = "dev"
    }
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.52.0"
    }
  }




locals {
  project_name = "gettingStarted"
  owner        = "Janevieve"
}

#creating aws instance
resource "aws_instance" "web" {
  ami           = "ami-0aa7d40eeae50c9a9"
  instance_type = var.instance_type
  tags = {
    Name = "web-${local.project_name}"
  }
}

