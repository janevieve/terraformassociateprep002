terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.52.0"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}
variable "instance_type" {
  type    = string
}

locals {
  project_name = "Demo"
  owner        = "Community Team"
}

#creating aws instance
resource "aws_instance" "web" {
  ami           = "ami-0aa7d40eeae50c9a9"
  instance_type = var.instance_type
  tags = {
    Name = "web-${local.project_name}"
  }
}

output "instance_ip_addr" {
  value = aws_instance.web.public_ip
}
