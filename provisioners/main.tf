terraform {
  cloud {
    organization = "fashiondeluxe"

    workspaces {
      name = "provisioners"
    }
  }
}

terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.52.0"
    }
  }
}

provider "aws" {
    region = "us-east-1"
  # Configuration options
}

#creating aws instance
resource "aws_instance" "web" {
  ami           = "ami-0aa7d40eeae50c9a9"
  instance_type = "t2.micro"
  tags = {
    Name = "web"
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key =  ""
}


output "private_key" {
  value     = tls_private_key.example.private_key_pem
  sensitive = true
}